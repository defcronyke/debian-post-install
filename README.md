Debian Post-Install Setup Script  
================================  
  
[https://gitlab.com/defcronyke/debian-post-install](https://gitlab.com/defcronyke/debian-post-install)  
  
Copyright © 2021 Jeremy Carter <jeremy@jeremycarter.ca>  
  
License: MIT License  
  
By using this software, you agree to the LICENSE TERMS 
outlined in the file titled LICENSE.md contained in the 
top-level directory of this project. If you don't agree
to the LICENSE TERMS, you aren't allowed to use this
software.  
  
  
Purpose and Usage:  
------------------  
  
Run this script after installing Debian to automatically 
set up a few things which people sometimes complain about 
Debian not doing during its official install process.  
  
There's no need to download it from here first, you can 
run the script by just opening a Terminal on Debian and
running the following command:  
```shell
su - -c 'apt-get update && apt-get install -y wget' && \
bash <(wget -qO - https://bit.ly/debian-post-install)
```  
  
It will prompt you for your root password to execute the 
first line, and then again to execute the second line. 
Note that this is your root password, not your regular 
user's password.  
  
If you happen to already have `wget` installed, you can just 
run the second of the above two lines.  
  
  
The script currently does the following setup steps:  
----------------------------------------------------  
  
1. Add the current user to sudo group.  
  
2. Enable the official Debian contrib and non-free apt 
repositories if they aren't already enabled.  
  
3. If on an x86_64 system, enable the i386 architecture so 
that if you want to install wine later, it might work 
properly for more things.  
  
4. Update the system, including any available dist-upgrade 
packages in the current Debian release.  
  
5. Install all available free and non-free device firmware.  
  
6. Enable the official backports repository, so you can 
install some newer software when you want to by using
the command:  
```shell
sudo apt-get -t `lsb_release -cs`-backports install <package name>
```  
  
7. Install the correct nvidia video card driver and related 
packages if you have an nvidia GPU. This will install the 
latest version of the driver and linux kernel from the 
backports apt repository, for best GPU compatibility.  
  
  
See the script here if you want to verify what it's doing: 
[debian-post-install.sh](https://gitlab.com/defcronyke/debian-post-install/-/blob/master/debian-post-install.sh)  
  
  
Optional:  
---------  
  
1. If you have an nvidia GPU, but you don't want to install the
nvidia driver, you can skip that part by setting the environment
variable `NO_NVIDIA=y`. For example:  
```shell
su - -c 'apt-get update && apt-get install -y wget' && \
NO_NVIDIA=y bash <(wget -qO - https://bit.ly/debian-post-install)
```  
  
2. You can skip enabling and using the backports repository if
you don't want it, by setting the environment variable 
`NO_BACKPORTS=y`. If you have an nvidia GPU and you use this
option, you will get the nvidia driver from the stable repository
instead of the backports version, and the backports Linux kernel
will not be installed. For example:  
```shell
su - -c 'apt-get update && apt-get install -y wget' && \
NO_BACKPORTS=y bash <(wget -qO - https://bit.ly/debian-post-install)
```  
  
3. You can skip installing the extra free and non-free device
firmware if you don't think you'll be needing it or if you
prefer to not be using non-free firmware, by setting the 
environment variable `NO_FIRMWARE=y`. Note that if you have
an nvidia GPU though and you don't opt-out of installing the
nvidia driver as per the instructions above, the non-free
drivers contained in the `firmware-misc-nonfree` Debian package 
will still be installed anyway, since they are needed for some
nvidia GPUs to work properly. For example:  
```shell
su - -c 'apt-get update && apt-get install -y wget' && \
NO_FIRMWARE=y bash <(wget -qO - https://bit.ly/debian-post-install)
```  
  
4. You can skip any or all of the things mentioned in this 
section above, by setting multiple environment variables.
For example:  
```shell
su - -c 'apt-get update && apt-get install -y wget' && \
NO_NVIDIA=y NO_BACKPORTS=y NO_FIRMWARE=y bash <(wget -qO - https://bit.ly/debian-post-install)
```  
  
  
Tips for achieving the best Debian out-of-the-box experience:  
-------------------------------------------------------------  
  
* I always recommend using the Xfce desktop environment on
desktop Linux installations for the least chance of problems.
It's not too heavy, but you can add lots of extras to it as
you prefer, and it's fairly customizable and rock solid.  
  
* The Debian website publishes an unofficial Live CD installer 
version which includes all the available non-free device firmware 
which isn't present in their official installation images. To 
avoid a potential situation where some of your hardware is not 
working properly during or immediately after Debian installation, 
I suggest that you consider using this unofficial installation
image instead of their official one. It's a bit hard to find
by navigating their website, so I'm linking to it here for
convenience:  
[https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/amd64/iso-hybrid/](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/amd64/iso-hybrid/)  
  
* You should only use the above images if you are okay with 
installing and using non-free device firmware which cannot
be audited properly for safety and security issues, since
the firmware source code isn't available from the manufacturers
of the associated devices. If you'd rather not be doing that
you can usually use their official installation images without
any problems, which you can find here:  
[https://www.debian.org/distrib/](https://www.debian.org/distrib/)  
  
