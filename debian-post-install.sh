#!/bin/bash
# Copyright © 2021 Jeremy Carter <jeremy@jeremycarter.ca>
#
# License: MIT License
#
# By using this software, you agree to the LICENSE TERMS 
# outlined in the file titled LICENSE.md contained in the 
# top-level directory of this project. If you don't agree
# to the LICENSE TERMS, you aren't allowed to use this
# software.

# Supported optional environment variables:
#
#   NO_NVIDIA=y
#   NO_BACKPORTS=y
#   NO_FIRMWARE=y
#

debian_post_install_sh() { \
  su - -w NO_NVIDIA,NO_BACKPORTS,NO_FIRMWARE -c '\
  echo -e "\nStarting setup...\n"; \
  if [ ! -z ${NO_NVIDIA+x} ]; then echo "Optional setting activated: NO_NVIDIA"; fi; \
  if [ ! -z ${NO_BACKPORTS+x} ]; then echo "Optional setting activated: NO_BACKPORTS"; fi; \
  if [ ! -z ${NO_FIRMWARE+x} ]; then echo "Optional setting activated: NO_FIRMWARE"; fi; \
  echo; \
  gpasswd -a '$USER' sudo; \
  cat /etc/apt/sources.list | grep "contrib non-free" || sed -i "s/main/main contrib non-free/g" /etc/apt/sources.list; \
  uname -a | grep "x86_64"; \
  if [ $? -eq 0 ]; then dpkg --add-architecture i386; fi; \
  apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y && apt-get autoremove -y && apt-get install -y lsb-release; \
  if [ -z ${NO_FIRMWARE+x} ]; then \
    apt-get install -y firmware-linux firmware-misc-nonfree; \
  fi; \
  export DEBIAN_RELEASE_NAME=`lsb_release -cs`; \
  if [ -z ${NO_BACKPORTS+x} ]; then \
    export DEBIAN_BACKPORTS_OPTS="-t ${DEBIAN_RELEASE_NAME}-backports"; \
    mkdir -p /etc/apt/sources.list.d && \
    echo -e "deb http://deb.debian.org/debian ${DEBIAN_RELEASE_NAME}-backports main contrib non-free\ndeb-src http://deb.debian.org/debian ${DEBIAN_RELEASE_NAME}-backports main contrib non-free\n" | \
      tee /etc/apt/sources.list.d/debian-backports.list && \
    apt-get update; \
  fi; \
  if [ -z ${NO_NVIDIA+x} ]; then \
    apt-get ${DEBIAN_BACKPORTS_OPTS} install -y nvidia-detect; \
    export NVIDIA_DRIVER_PACKAGE=`nvidia-detect 2>/dev/null | tail -n 2 | head -n 1 | xargs`; \
    echo "$NVIDIA_DRIVER_PACKAGE" | grep nvidia-; \
    if [ $? -eq 0 ]; then \
      export DEBIAN_ARCH_NAME=`uname -r | rev | cut -d- -f1 | rev`; \
      export DEBIAN_ARCH_NAME_FALLBACK=`uname -r | rev | cut -d- -f1,2 | rev`; \
      apt-get ${DEBIAN_BACKPORTS_OPTS} install -y linux-image-${DEBIAN_ARCH_NAME} linux-headers-${DEBIAN_ARCH_NAME} || \
      apt-get ${DEBIAN_BACKPORTS_OPTS} install -y linux-image-${DEBIAN_ARCH_NAME_FALLBACK} linux-headers-${DEBIAN_ARCH_NAME_FALLBACK}; \
      if [ -z ${NO_FIRMWARE+x} ] && [ -z ${NO_BACKPORTS+x} ]; then \
        apt-get ${DEBIAN_BACKPORTS_OPTS} install -y firmware-linux firmware-misc-nonfree; \
      fi; \
      if [ ! -z ${NO_FIRMWARE+x} ]; then \
        apt-get ${DEBIAN_BACKPORTS_OPTS} install -y firmware-misc-nonfree; \
      fi; \
      apt-get ${DEBIAN_BACKPORTS_OPTS} install -y build-essential ${NVIDIA_DRIVER_PACKAGE} && \
      apt-get autoremove -y && \
      echo "Setup finished. You need to reboot." && \
      exit 0 || \
      echo "Installing nvidia drivers failed. Everything else should be fine, but you might want to install the nvidia drivers yourself afterwards."; \
    fi; \
  fi; \
  echo "Setup finished. You need to logout and log back in to enable sudo support."'; \
}; \
debian_post_install_sh
